<?php

namespace studiosite\yii2touchswipe;

use Yii;
use yii\web\AssetBundle;

/**
 * Бадл TouchSwipe
 *
 * @copyright Студия.сайт
 * @author fromtuba <fromtuba@mail.ru>
 */
class TouchSwipeAsset extends AssetBundle
{
	/**
    * @var string Альяс пути где находятся асетсы
    */
	public $baseUrl = '@web';

	/**
    * @var array Исходный путь
    */
    public $sourcePath = '@bower/jquery-touchswipe';

	/**
    * @var array Список файлов стилей по порядку подключения
    */
    public $css = [
    ];

	/**
    * @var array Список файлов JS файлов по порядку подключения
    */
    public $js = [
    	'jquery.touchSwipe.js',
    ];

    /**
    * @var array Список асетсов - зависимости текущего асетса
    */
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
        'yii\bootstrap\BootstrapPluginAsset',
    ];
}