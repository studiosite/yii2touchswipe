TouchSwipe
=================

Ассетс бадл [TouchSwipe](https://github.com/mattbryson/TouchSwipe-Jquery-Plugin)
## Установка

Предпочтительный способ установить это расширение через композитор. [composer](http://getcomposer.org/download/).

```
"studiosite/yii2swipenav": "*"
```

в секции ```require``` `composer.json` файла.

В секции ```repositories``` добавить

```
{
    "type": "vcs",
    "url": "https://bitbucket.org/studiosite/yii2touchswipe"
}
```

## Использование

В отображении зарегистрировать бандл

```php
/**
* @var $this \yii\web\View
*/
\studiosite\yii2touchswipe\TouchSwipeAsset::register($this);
```